import json
import os
from google_auth_oauthlib.flow import InstalledAppFlow
from google.oauth2.credentials import Credentials
from google.auth.transport.requests import Request

credentials = None
__G_DRIVE_TOKEN_FILE = "token.json"
__OAUTH_SCOPE = ["https://www.googleapis.com/auth/drive"]
if os.path.exists(__G_DRIVE_TOKEN_FILE):
    credentials = Credentials.from_authorized_user_file(__G_DRIVE_TOKEN_FILE)
    if credentials is None or not credentials.valid:
        if credentials and credentials.expired and credentials.refresh_token:
            credentials.refresh(Request())
else:
    flow = InstalledAppFlow.from_client_secrets_file(
        'credentials.json', __OAUTH_SCOPE)
    credentials = flow.run_local_server(port=0)

# Save the credentials for the next run
with open(__G_DRIVE_TOKEN_FILE, 'wt') as token:
    json.dump(json.loads(credentials.to_json()), token)
