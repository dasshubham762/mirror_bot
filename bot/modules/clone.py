from typing import Text
from urllib.parse import ParseResult
from telegram.ext import CommandHandler
from telegram import InlineKeyboardMarkup
from bot.helper.mirror_utils.upload_utils.gdriveTools import GoogleDriveHelper
from bot.helper.telegram_helper.message_utils import *
from bot.helper.telegram_helper.filters import CustomFilters
from bot.helper.telegram_helper.bot_commands import BotCommands
from bot.helper.ext_utils.bot_utils import new_thread
from bot import dispatcher


@new_thread
def cloneNode(update, context):
    args = update.message.text.split(" ", maxsplit=1)
    button = None
    if len(args) > 1:
        link = args[1]
        msg = sendMessage(f"Cloning: <code>{link}</code>", context.bot, update)
        gd = GoogleDriveHelper()
        result = gd.clone(link)
        deleteMessage(context.bot, msg)
        if type(result) == tuple:
            text = result[0]
            button = result[1]
        else:
            text = result
        sendMessage(text=text, bot=context.bot, update=update, button=button)
    else:
        sendMessage("Provide G-Drive Shareable Link to Clone.",
                    context.bot, update)


clone_handler = CommandHandler(BotCommands.CloneCommand, cloneNode,
                               filters=CustomFilters.authorized_chat | CustomFilters.authorized_user)
dispatcher.add_handler(clone_handler)
